//Load fonts
WebFont.load({
  google: {
    families: ['Material+Icons', 'Lato:300,400,700']
  }
});
$(document).ready(function () {

  //Tooltips
  $('[data-toggle="popover"]').popover()

  //Icons
  $('.verify-icon').addClass('bx bxs-shield bx-md');
  $('.time-icon').addClass('bx bxs-time bx-md');
  $('.plus-icon').addClass('bx bx-plus bx-md');
  $('.send-icon').addClass('bx bxs-send bx-md');
  $('.warning-icon').addClass('bx bxs-error bx-sm');
  $('.pro-agent-icon').addClass('bx bxs-trophy-star bx-sm');
  $('.link-external-icon').addClass('bx bx-link-external bx-md');
  $('.whatsapp-icon').addClass('bx bxl-whatsapp bx-md');
  $('.verify-icon.icon-sm').removeClass('bx-md').addClass('bx-sm');
  $('.time-icon.icon-sm').removeClass('bx-md').addClass('bx-sm');

  //Toggle listing data in mobile devices
  if ($(document).width() <= 767) {
    var tradingItem = $('.trading-item-data');
    tradingItem.click(function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('expanded-item');
      $(this).parent().find('.pay-options.show').toggleClass('show');
    });
    $('[data-toggle="tooltip"]:not(.pay-method)').tooltip();
    $('[data-toggle="tooltip"].pay-method').tooltip({
      trigger: 'focus'
    });
  } else {
    $('[data-toggle="tooltip"]').tooltip();
  }

  //Payment options toggle
  var paymentOption = $('.trading-item .pay-method');
  paymentOption.click(function (e) {
    e.preventDefault();
    var paysContent = $(this).attr('data-options');
    var parentPosition = $(this).attr('data-parent-position');
    var theOptions = $('#item-' + parentPosition + '-pay-options-' + paysContent);
    if (theOptions) {
      $('.trading-item .pay-options.show').toggleClass('show');
      theOptions.toggleClass('show');
    }
  });
  var payOptionsClose = $('.trading-item .pay-options .close');
  payOptionsClose.click(function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('show');
  });

  //Dev only script delete in production
  $('#leave-wesend-popup').modal('show').find('.btn-send').click(function (e) {
    e.preventDefault();
    $('#leave-wesend-popup .step-1').hide();
    $('#leave-wesend-popup .step-2').show();
  });
});
