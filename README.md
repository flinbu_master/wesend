# WeSend P2P HTML
## Pre-requisities
Install NPM and Bower
```
npm i -g bower
```
## Installation
Run `npm install` post installation the command `bower install` will auto run

## Commands
`gulp build`
Compile and build the project

`gulp clean`
Remove `build` folder

`gulp`
Run `gulp build` - `gulp watch` and a `browsersync` instance

`gulp css`
Compiles SCSS

`gulp js`

`gulp html`

`gulp images`
Copy and minify images in `src/img` to `build/assets/img`

`gulp json-deps`

`gulp fonts`

`gulp flags`
Copy SVG flags files into `build/assets/img/flags`

`gulp font-face`
Create a CSS file into `build/assets/css` with fonts in `src/font2css` folder

## Libraries
### [Bootstrap 3.3.7 SASS Port](https://github.com/twbs/bootstrap-sass)
### [Bootstrap Select](https://developer.snapappointments.com/bootstrap-select/)
### [Boxicons](https://boxicons.com/)
