'use strict';

//Project config
const project = {
  title: 'WeSend',
  name: 'wesend',
  slug: 'wesend',
  filenames: {
    js: 'wesend',
    css: 'wesend'
  },
  serve: {
    proxy: false,
    dir: 'build',
    url: 'http://localhost/wesend/build'
  },
  dependencies: {
    css: [
      {
        src: 'node_modules/boxicons/css/boxicons.min.css',
        build: 'build/assets/css'
      }
    ],
    js: [
      {
        download:
          'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js',
        build: 'build/assets/js'
      },
      {
        src: 'node_modules/jquery/dist/jquery.min.js',
        build: 'build/assets/js'
      },
      {
        src: 'node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        build: 'build/assets/js'
      },
      {
        src: 'node_modules/withinviewport/jquery.withinviewport.js',
        build: 'build/assets/js'
      },
      {
        src: 'node_modules/bootstrap-select/dist/js/bootstrap-select.min.js',
        build: 'build/assets/js'
      }
    ],
    images: false,
    fonts: [
      {
        src: 'node_modules/boxicons/fonts/**/*',
        build: 'build/assets/fonts'
      },
      {
        src: 'node_modules/bootstrap-sass/assets/fonts/**/*',
        build: 'build/assets/fonts'
      }
    ],
    icons: false,
    fonts2css: [
      {
        src: 'src/fonts2css/**/*.{otf,ttf,woff,woff2}',
        build: 'build/assets/css'
      }
    ],
    json: [
      {
        src: 'src/data/countries.json',
        name: 'countries.json',
        build: 'build/assets/data'
      }
    ]
  }
};

/////////////// DON'T MODIFIED FROM THIS POINT //////////////////////
var dir = {
  src: 'src',
  build: 'build'
};
var gulp = require('gulp');
var gutil = require('gulp-util');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var deporder = require('gulp-deporder');
var concat = require('gulp-concat');
var stripdebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var include = require('gulp-file-include');
var download = require('gulp-download');
var font2css = require('gulp-font2css').default;
var changeCase = require('change-case');
var clean = require('gulp-clean');

//Browser Sync
var browserSync = false;
var syncOpts;
if (project.serve.proxy) {
  syncOpts = {
    proxy: project.serve.url,
    files: dir.build + '/**/*',
    open: true,
    notify: true,
    logLevel: 'debug',
    ui: {
      post: 80001
    }
  };
} else {
  syncOpts = {
    server: {
      baseDir: './' + dir.build
    },
    files: dir.build + '/**/*',
    open: true,
    notify: true,
    logLevel: 'debug',
    port: 5000,
    ui: {
      port: 8001
    }
  };
}

//HTML Settings
var html = {
  src: [dir.src + '/html/*.html', dir.src + '/html/**/*.php'],
  build: dir.build,
  watch: [
    dir.src + '/html/*.html',
    dir.src + '/html/**/*.php',
    dir.src + '/html/components/**/*'
  ]
};

//Images settings
var images = {
  src: dir.src + '/img/**/*',
  build: dir.build + '/assets/img',
  watch: dir.src + '/img/**/*',
  dependencies: false
};
if (project.dependencies.images) {
  images.dependencies = project.dependencies.images;
}

//Images settings
var images = {
  src: dir.src + '/img/**/*',
  build: dir.build + '/assets/img',
  watch: dir.src + '/img/**/*',
  dependencies: false
};
if (project.dependencies.images) {
  images.dependencies = project.dependencies.images;
}

//Fonts settings
var fonts = {
  src: dir.src + '/fonts/**/*',
  build: dir.build + '/assets/fonts',
  watch: dir.src + '/fonts/**/*',
  needs: project.dependencies.fonts,
  dependencies: false
};
if (project.dependencies.fonts) {
  fonts.dependencies = project.dependencies.fonts;
}

//JS Settings
var js = {
  src: dir.src + '/js/**/*.js',
  build: dir.build + '/assets/js',
  watch: dir.src + '/js/**/*.js',
  dependencies: false
};
if (project.dependencies.js) {
  js.dependencies = project.dependencies.js;
}

var injectJS = '';
project.dependencies.js.forEach(function(dep) {
  var depSrc = dep.src ? dep.src : dep.download;
  var depSplit = depSrc.split('/');
  var depName = depSplit[depSplit.length - 1];
  injectJS =
    injectJS + '<script src="assets/js/' + depName + '"></script>' + '\n';
});
injectJS =
  injectJS +
  '<script src="assets/js/' +
  project.filenames.js +
  '.js"></script>';

//CSS Settings
var css = {
  src: dir.src + '/scss/app.scss',
  build: dir.build + '/assets/css',
  watch: dir.src + '/scss/**/*.scss',
  sassOpts: {
    outputStyle: 'nested',
    imagePath: images.build,
    precision: 3,
    errLogToConsole: true
  },
  processors: [
    require('postcss-assets')({
      loadPaths: ['/assets/images'],
      basePath: dir.build,
      baseUrl: images.build
    }),
    require('autoprefixer')({
      browsers: ['last 2 versions', '> 2%']
    }),
    require('css-mqpacker'),
    require('cssnano')
  ],
  dependencies: false
};
if (project.dependencies.css) {
  css.dependencies = project.dependencies.css;
}

var injectCSS =
  '<link rel="stylesheet" href="assets/css/' +
  project.filenames.css +
  '.css" type="text/css" />';
var fontsFile =
  '<link rel="stylesheet" href="assets/css/' +
  project.filenames.css +
  '.fonts.css" type="text/css" />';

///////// TASKS ///////////
//Image processing
gulp.task('images-deps', () => {
  if (images.dependencies) {
    images.dependencies.forEach(function(dep) {
      if (dep.src) {
        gulp
          .src(dep.src)
          .pipe(newer(dep.build))
          .pipe(imagemin())
          .pipe(gulp.dest(dep.build));
      }
      if (dep.download) {
        download(dep.download)
          .pipe(imagemin())
          .pipe(gulp.dest(dep.build));
      }
    });
  }
});
gulp.task('images', ['images-deps'], () => {
  gulp
    .src(images.src)
    .pipe(newer(images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(images.build));
});

//CSS Processing
gulp.task('css-deps', () => {
  if (css.dependencies) {
    css.dependencies.forEach(function(dep) {
      if (dep.src) {
        gulp
          .src(dep.src)
          .pipe(newer(dep.build))
          .pipe(gulp.dest(dep.build));
      }
      if (dep.download) {
        download(dep.download).pipe(gulp.dest(dep.build));
      }
    });
  }
});
gulp.task('css', ['images'], () => {
  gulp
    .src(css.src)
    .pipe(sourcemaps.init())
    .pipe(sass(css.sassOpts))
    .pipe(postcss(css.processors))
    .pipe(rename(project.filenames.css + '.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(css.build));
});

//HTML Processing
gulp.task('html', () => {
  gulp
    .src(html.src)
    // .pipe(newer({
    //     dest: html.build,
    //     extra: html.build + '/modules/**/*'
    // }))
    .pipe(
      include({
        context: {
          CSSFile: injectCSS,
          JSFile: injectJS,
          FontsFile: fontsFile,
          title: project.title,
          OGData: ''
        }
      })
    )
    .pipe(gulp.dest(html.build));
});

//JS Processing
gulp.task('js-deps', () => {
  if (js.dependencies) {
    js.dependencies.forEach(function(dep) {
      if (dep.src) {
        gulp.src(dep.src).pipe(gulp.dest(dep.build));
      }
      if (dep.download) {
        download(dep.download).pipe(gulp.dest(dep.build));
      }
    });
  }
});
gulp.task('js', () => {
  gulp
    .src(js.src)
    .pipe(deporder())
    .pipe(concat(project.filenames.js + '.js'))
    .pipe(stripdebug())
    .pipe(gulp.dest(js.build))
    .pipe(rename(project.filenames.js + '.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(js.build));
});

gulp.task('json-deps', () => {
  if (project.dependencies.json) {
    project.dependencies.json.forEach(function(dep) {
      if (dep.src) {
        gulp.src(dep.src)
            .pipe(rename(dep.name))
            .pipe(gulp.dest(dep.build));
      }
      if (dep.download) {
        download(dep.download)
          .pipe(rename(dep.name))
          .pipe(gulp.dest(dep.build));
      }
    });
  }
});

//Fonts2CSS
gulp.task('font-face', () => {
  if (project.dependencies.fonts2css) {
    project.dependencies.fonts2css.forEach(function(dep) {
      gulp
        .src(dep.src)
        .pipe(font2css())
        .pipe(concat(project.filenames.css + '.fonts.css'))
        .pipe(postcss(css.processors))
        .pipe(gulp.dest(dep.build));
    });
  }
});

//Fonts Processing
gulp.task('fonts', () => {
  gulp
    .src(fonts.src)
    .pipe(newer(fonts.build))
    .pipe(gulp.dest(fonts.build));

  if (fonts.needs) {
    fonts.needs.forEach(function(dep) {
      if (dep.src) {
        gulp.src(dep.src).pipe(gulp.dest(dep.build));
      }
      if (dep.download) {
        download(dep.download).pipe(gulp.dest(dep.build));
      }
    });
  }
});

//Flags
gulp.task('flags', () => {
  gulp
    .src('node_modules/svg-country-flags/svg/**/*')
    .pipe(rename(function(path) {
      var fileUC = changeCase.upperCase(path.basename);
      path.basename = fileUC;
    }))
    .pipe(gulp.dest('build/assets/img/flags'));
});

//Clean
gulp.task('clean', () => {
  gulp.src('build', {
    read: false
  }).pipe(clean({
    force: true
  }));
});

//Build
gulp.task('build', [
  'html',
  'css',
  'js-deps',
  'js',
  'json-deps',
  'fonts',
  'font-face',
  'css-deps',
  'flags'
]);

//Browser Sync
gulp.task('browsersync', () => {
  if (!browserSync) {
    browserSync = require('browser-sync').create();
    browserSync.init(syncOpts);
  }
});

//Watch
gulp.task('watch', ['browsersync'], () => {
  //HTML
  gulp.watch(html.watch, ['html']);

  //Images
  gulp.watch(images.watch, ['images']);

  //CSS
  gulp.watch(css.watch, ['css']);

  //Fonts
  gulp.watch(fonts.watch, ['fonts']);

  //JS
  gulp.watch(js.watch, ['js']);
});

gulp.task('watch-html', () => {
  gulp.watch(html.watch, ['html']);
});

//Defautl
gulp.task('default', ['build', 'watch']);
